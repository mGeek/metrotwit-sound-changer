# MetroTwit Sound Changer

- - -

###Qu'est-ce que c'est ?

MetroTwit Sound Changer (aka: MTSC) est une interface qui permet de choisir un nouveau son de notification pour le client Twitter pour Windows: MetroTwit.

Testé uniquement avec les versions suivantes :

  - _MetroTwit for Windows_
  - _MetroTwit Plus for Windows_ (plusieurs profils)
  - _MetroTwit Loop_
 
Non testé avec _MetroTwit for Windows 8_.

### Téléchargement :

  - [MetroTwit Sound Changer 0.2](https://bitbucket.org/mGeek/metrotwit-sound-changer/src/7cb24d70d50be24a20f08084c256d8313a4d6d38/MetroTwit%20Sound%20Changer.exe?at=master) (dernière version disponible)
  - [MetroTwit Sound Changer 0.1.1](https://bitbucket.org/mGeek/metrotwit-sound-changer/src/e0728d7533f0463edfc9bf6c2f9a81cff288f977/MetroTwit%20Sound%20Changer.exe?at=master)

[Changelog](https://bitbucket.org/mGeek/metrotwit-sound-changer/src/7cb24d70d50be24a20f08084c256d8313a4d6d38/CHANGELOG.md?at=master)

### Source :

Projet codé en [AutoIt3](http://www.autoitscript.com/site/autoit/) et commenté un minimum. Vous devez posséder l'interpréteur pour exécuter la source.

### Comment ça marche :

Le son "beep.mp3" de MetroTwit se retrouve à plusieurs endroits dans des dossiers du genre :
    
    ..\2.0\AMGEVNM7.CQ1\H4YP879Z.Q8C\metr...exe_72b2aef66840e297_0001.0001_en-au_4f74b94b0a17c8e4\Resources
    
Avec bien évidemment un code qui diffère sur installation du logiciel.
Le script se charge de retrouver ces fichiers, puis de les remplacer par celui sélectionné.

- - -

Des questions ? [twitter.com/mGeek_](http://twitter.com/mGeek_)

.. et pour tout le reste : [mgeek.fr](http://mgeek.fr/)