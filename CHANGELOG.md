# Changelog

- - -

## 0.2

- Bug: Quand "beep.mp3" était présent dans plusieurs dossiers, un seul était remplacé et les autres supprimés.
- Ajout d'un nouveau son de notification (TweetBot.mp3)

## 0.1

- First commit