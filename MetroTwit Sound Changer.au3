#NoTrayIcon

#AutoIt3Wrapper_Icon=img\MTSC.ico
#AutoIt3Wrapper_UseX64=n
#AutoIt3Wrapper_Res_Fileversion=0.0.0.0

;MetroTwit Sound Changer 0.2
;Cr�� par mGeek (mgeek.fr)
;Changelog disponible dans le CHANGELOG.md

#include <FileListToArrayNT.au3>
#include <GUIConstantsEx.au3>

;Recherche des dossiers contenant le son de notification de MetroTwit
$mp3_paths = _FileListToArrayNT(@UserProfileDir & "\AppData\Local\Apps\2.0", "*.mp3", 0, 2, True, "Temp")
If @error Then MsgBox(16, "Erreur", "Aucune installation de MetroTwit compatible n'a �t� trouv�e")

;Extraction des ressources
DirCreate(@TempDir & "\MTSC")
FileInstall("sounds/Basso.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/Block.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/Funk.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/Marimba.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/TweetDeck.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/TweetBot.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("sounds/Sosumi.mp3", @TempDir & "\MTSC\*", 1)
FileInstall("img/background.jpg", @TempDir & "\MTSC\*", 1)

;Fen�tre
GUICreate("MTSC 0.2", 250, 280)
GUISetBkColor(0x222222)
GUISetFont(9, -1, -1, "Segoe UI")
GUICtrlCreatePic(@TempDir & "\MTSC\background.jpg", 0, 0, 250, 300)
GUICtrlSetState(-1, $GUI_DISABLE)

GUICtrlCreateLabel("Son actuel :", 20, 80, 70, 20)
GUICtrlSetColor(-1, 0xFFFFFF)
$b_listen_current = GUICtrlCreateButton("�couter", 90, 75, 70, 25)

GUICtrlCreateLabel("", 25, 120, 200, 1) ;S�parateur
GUICtrlSetBkColor(-1, 0xAAAAAA)

$b_browse_mp3 = GUICtrlCreateButton("Choisir un nouveau son ...", 250/2-150/2, 135, 150, 25)
GUICtrlCreateLabel("Nouveau son s�lectionn� :", 20, 180, 150, 20)
GUICtrlSetColor(-1, 0xFFFFFF)
$b_listen_new = GUICtrlCreateButton("�couter", 165, 175, 70, 25)
GUICtrlSetState(-1, $GUI_DISABLE)

GUICtrlCreateLabel("", 25, 220, 200, 1) ;S�parateur
GUICtrlSetBkColor(-1, 0xAAAAAA)

$b_start_change = GUICtrlCreateButton("Valider", 250/2-100/2, 240, 100, 25)
GUICtrlSetState(-1, $GUI_DISABLE)

GUISetState(@SW_SHOW)

;Boucle principale
$mp3_to_install = ""
While 1
	Switch GUIGetMsg()
		Case $GUI_EVENT_CLOSE
			Exit
		Case $b_listen_current
			SoundPlay($mp3_paths[1])
		Case $b_browse_mp3
			$f_browse = FileOpenDialog("S�lectionner un son", @TempDir & "\MTSC", "Fichiers MP3 (*.mp3)")
			If FileExists($f_browse) Then
				GUICtrlSetState($b_listen_new, $GUI_ENABLE)
				GUICtrlSetState($b_start_change, $GUI_ENABLE)
				$mp3_to_install = $f_browse
			Else
				GUICtrlSetState($b_listen_new, $GUI_DISABLE)
				GUICtrlSetState($b_start_change, $GUI_DISABLE)
				$mp3_to_install = ""
			EndIf
		Case $b_listen_new
			SoundPlay($mp3_to_install)
		Case $b_start_change
			If ProcessExists("MetroTwit.exe") Then ProcessClose("MetroTwit.exe")
			ConsoleWrite("Copie de " & $mp3_to_install & " :" & @CRLF)
			For $i = 1 To $mp3_paths[0]
				FileDelete($mp3_paths[$i])
				FileCopy($mp3_to_install, $mp3_paths[$i])
				If @error Then ConsoleWrite("!> Erreur !" & @CRLF)
				ConsoleWrite("   Copi� dans " & $mp3_paths[$i] & @CRLF)
			Next
			MsgBox(64, "MTSC", "Son de notification chang� avec succ�s !" & @CRLF & "Relancez MetroTwit pour l'essayer.")
			Exit
	EndSwitch
	Sleep(1)
WEnd
